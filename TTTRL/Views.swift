//
//  View.swift
//  TTTRL
//
//  Created by Michael Michailidis on 17/02/2017.
//  Copyright © 2017 Shenzhen Huazhongxiang. All rights reserved.
//

import UIKit

class CellView: UIView {
    
    var mark:Mark = .E {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        context.setStrokeColor(UIColor.white.cgColor)
        context.setLineCap(.round)
        context.setLineWidth(4.0)
        
        let drawX = { (irect: CGRect) in
            
            context.move(to: irect.origin)
            context.addLine(to: CGPoint(x: irect.origin.x + irect.size.width, y: irect.origin.y + irect.size.height))
            context.strokePath()
            
            context.move(to: CGPoint(x: irect.origin.x, y: irect.origin.y + irect.size.height))
            context.addLine(to: CGPoint(x: irect.origin.x + irect.size.width, y: irect.origin.y))
            context.strokePath()
            
        }
        
        let drawO = { (irect: CGRect) in
            
            let center = CGPoint(x: irect.midX, y: irect.midY);
            context.beginPath()
            context.addArc(center: center, radius: irect.size.width / 2.0 + 4.0 , startAngle: 0.0, endAngle: CGFloat(2.0 * Double.pi), clockwise: true)
            context.strokePath()
            
        }
        
        let inframe = rect.insetBy(dx: 20.0, dy: 20.0)
     
        switch mark {
        case .X:
            drawX(inframe)
            break
        case .O:
            drawO(inframe)
            break
        case .E:
            return
        }
        
    }
    
}

class BoardView: UIView {
    
    var model:Board?
    
    class BGHashView: UIView {
        
        static let Inset:CGFloat = 4.0
        
        override func draw(_ rect: CGRect) {
            
            let inframe = rect.insetBy(dx: 4.0, dy: 4.0)
            
            var fr   = inframe
            fr.size /= (Board.Width, Board.Height)
            
            guard let context = UIGraphicsGetCurrentContext() else {
                return
            }
            
            context.setStrokeColor(UIColor.white.cgColor)
            context.setLineCap(.round)
            context.setLineWidth(5.0)
            
            var pen: CGPoint = CGPoint()
            
            for _ in 0..<2 {
                
                pen.y   = inframe.origin.y
                pen.x  += fr.width
                context.move(to: pen)
                
                pen.y  += inframe.size.height
                context.addLine(to: pen)
                context.strokePath()
                
            }
            
            pen.y   = inframe.origin.y
            
            for _ in 0..<2 {
                
                pen.x   = inframe.origin.x
                pen.y  += fr.height
                context.move(to: pen)
                
                pen.x  += inframe.size.width
                context.addLine(to: pen)
                context.strokePath()
                
            }
            
        }
        
    }

    static let Inset:CGFloat = 4.0
    
    var cells:[CellView]        = [CellView]()
    var hview:BGHashView        = BGHashView()
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.hview.backgroundColor = UIColor.clear
        self.addSubview(self.hview)
      
        for _ in 0..<(Board.Width*Board.Height) {
            let cell = CellView()
            self.cells.append(cell)
            cell.backgroundColor = UIColor.clear
            self.addSubview(cell)
        }
        
    }
    
    func updateSubviews(with model: Board) {
        
        for row in 0..<Board.Width {
            for col in 0..<Board.Height {
                self[row,col].mark = model[row,col]
            } 
        }
        
        self.model = model
        
    }
    
    var animating:Bool = false
    func showVictory(_ pattern:(Int,Int,Int), complete: (() -> Void)? = nil) {
        
        animating = true
        
        let cells = self.cells.filter {
            let index = self.cells.index(of: $0)
            return index == pattern.0 || index == pattern.1 || index == pattern.2
        }
        
        
        let a_dur:TimeInterval = 0.8
        var start:TimeInterval = 0.0
        UIView.animateKeyframes(withDuration: a_dur, delay: 0.0, options: UIViewKeyframeAnimationOptions.beginFromCurrentState, animations: {
            
            func addInsetKeyframe(with relativeStartTime:TimeInterval, and relativeDuration:TimeInterval, for inset:CGFloat) -> TimeInterval {
                
                UIView.addKeyframe(withRelativeStartTime: relativeStartTime, relativeDuration: relativeDuration, animations: {
                    for cell in cells {
                        cell.frame = cell.frame.insetBy(dx: inset, dy: inset)
                    }
                })
                return relativeDuration
            }
            
            start += addInsetKeyframe(with: start, and: 0.2, for: 1.0)
            start += addInsetKeyframe(with: start, and: 0.4, for: -11.0)
            start += addInsetKeyframe(with: start, and: 0.4, for: 10.0)
            
            
        }) { (finished) in
            
            self.animating = false
            complete?()
        }
    }
    
    func clearBoard(complete:(() -> Void)? = nil) {
        
        
        let a_dur:TimeInterval = 0.8
        let nudge:TimeInterval = 0.4
        let r_dur:TimeInterval = a_dur - nudge
        let r_off:TimeInterval = nudge / TimeInterval(self.cells.count)
        
        var start:TimeInterval = 0.0
        
        UIView.animateKeyframes(withDuration: a_dur, delay: 0.0, options: UIViewKeyframeAnimationOptions.beginFromCurrentState, animations: {
            
            for cell in self.cells {
                
                UIView.addKeyframe(withRelativeStartTime: start, relativeDuration: r_dur, animations: {
                    cell.alpha = 0.0
                })
                
                start += r_off
            }
            
        }) { (finished) in
            
            for cell in self.cells {
                cell.mark   = .E
                cell.alpha  = 1.0
            }
            
            complete?()
            
        }
        
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        if animating {
            return
        }
        
        self.hview.frame    = self.bounds.insetBy(dx: BoardView.Inset, dy: BoardView.Inset)
        
        var cellframe       = self.hview.frame.insetBy(dx: BGHashView.Inset, dy: BGHashView.Inset)
        
        cellframe.origin.x  = BGHashView.Inset
        cellframe.size     /= (Board.Width, Board.Height)
        
        for row in 0..<Board.Width {
            
            for col in 0..<Board.Height {
                
                let cell    = self[row,col]
                cell.frame  = cellframe
                
                cellframe.origin.x += cellframe.size.width
            }
            
            cellframe.origin.x  = BGHashView.Inset
            cellframe.origin.y += cellframe.size.height
        }
    }
    
    subscript(row: Int, col: Int) -> CellView {
        get {
            return self.cells[row * Board.Width + col]
        }
        set {
            self.cells[row * Board.Width + col] = newValue
        }
    }

}

func /(left:CGSize, right:(Int,Int)) -> CGSize {
    let width   = left.width / CGFloat(right.0)
    let height  = left.height / CGFloat(right.0)
    return CGSize(width: width, height: height)
}

func /=(left:inout CGSize, right:(Int,Int)) {
    left = left / right
}
