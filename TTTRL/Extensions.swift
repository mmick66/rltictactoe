//
//  Extensions.swift
//  TTTRL
//
//  Copyright © 2017 Shenzhen Huazhongxiang. All rights reserved.
//

import UIKit

extension Int {
    static func random(upper bound:Int = Int.max) -> Int {
        return Int(arc4random_uniform(UInt32(bound)))
    }
    func random() -> Int {
        return Int(arc4random_uniform(UInt32(self)))
    }
    var string:String {
        return String(self)
    }
}

extension Float {
    static func random() -> Float {
        return Float(arc4random()) / Float(UInt32.max)
    }
}

extension CGRect {
    func scale(x:CGFloat, y:CGFloat) -> CGRect {
        var copy = self
        copy.size.width     *= x
        copy.size.height    *= y
        copy.origin.x       -= (self.size.width - copy.size.width) / 2.0
        copy.origin.y       -= (self.size.width - copy.size.width) / 2.0
        return copy
    }
}

extension Array {
    
    var random:Element?  {
        get {
            switch self.count {
            case 0:
                return nil
            case 1:
                return self.first
            default:
                return self[Int.random(upper: self.count)]
            }
        }
    }
    
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        
        guard self.count > 1 else {
            return
        }
        
        var c = self.count
        
        while c > 0 {
            
            c -= 1
            
            let i = Int.random(upper: c)
            
            let t   = self[i]
            self[i] = self[c]
            self[c] = t
            
        }
    }
    
    var shuffled:Array {
        mutating get {
            self.shuffle()
            return self
        }
    }
    
    
}
