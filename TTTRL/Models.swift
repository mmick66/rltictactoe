//
//  Model.swift
//  TTTRL
//
//  Created by Michael Michailidis on 17/02/2017.
//  Copyright © 2017 Shenzhen Huazhongxiang. All rights reserved.
//

import Foundation

typealias Coordinates = (row:Int,col:Int)

enum Mark: String {
    case E = " "
    case X = "X"
    case O = "O"
    var oposite:Mark? {
        switch self {
        case .E:
            return nil
        case .X:
            return .O
        case .O:
            return .X
            
        }
    }
    static func ==(lhs:Mark, rhs:Mark) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
}



class Player {
    
    enum AI {
        case Random
        case Basic
        case Minimax
        case Learned
    }
    
    var score:Int = 0
    
    var strategy:Strategy?
    
    let mark:Mark
    init(with mark:Mark, and ai:Player.AI = .Random) {
        
        self.mark = mark
        
        var s:Strategy?
        
        switch ai {
        case .Random:
            s = RandomStrategy(with: self.mark)
        case .Basic:
            s = BasicStrategy(with: self.mark)
        case .Minimax:
            s = MinimaxStrategy(with: self.mark)
        case .Learned:
            s = RLStrategy(with: self.mark)
        }
        self.strategy = s
    }
    
    func choose(for board:Board) -> Coordinates? {
        
        guard let strategy = self.strategy else {
            return nil
        }
        
        return strategy.choose(in: board)
    }
    
    
}

extension Player: CustomStringConvertible {
    
    var description: String {
        return "<Player (mark:\"\(self.mark.rawValue)\", strategy:\(String(describing: self.strategy)))>"
    }
}

func ==(lhs:Player, rhs:Player) -> Bool {
    return lhs.mark == rhs.mark
}

struct Board {

    static let Width:Int    = 3
    static let Height:Int   = 3
    
    enum RotateDirection {
        case Clockwise
        case CounterClockwise
    }
    
    internal var data:[Mark] = Array<Mark>(repeating: .E, count: Board.Width * Board.Height)
    
    subscript(index:Int) -> Mark {
        get {
            return self.data[index]
        }
        set {
            self.data[index] = newValue
        }
    }
    subscript(row: Int, col: Int) -> Mark {
        get {
            return self.data[row * Board.Width + col]
        }
        set {
            self.data[row * Board.Width + col] = newValue
        }
    }
    
    mutating func play(mark:Mark, at coordinates:Coordinates) {
        self[coordinates.row,coordinates.col] = mark
    }
    mutating func rotated(times:Int) {
        
        for _ in 0..<times {
            var board = self
            self[0,0] = board[2,0]
            self[0,1] = board[1,0]
            self[0,2] = board[0,0]
            self[1,0] = board[2,1]
            self[1,1] = board[1,1] // center
            self[1,2] = board[0,1]
            self[2,0] = board[2,2]
            self[2,1] = board[1,2]
            self[2,2] = board[0,2]
        }
        
        
    }
    
    typealias Move = (coordinates: Coordinates, board: Board)
    func expand(for mark:Mark) -> [Move] {
        
        var possible = [Move]()
        
        for row in 0..<Board.Height {
            for col in 0..<Board.Width {
                
                if self[row,col] != .E {
                    continue
                }
                
                let coordinates = (row: row, col: col)
                
                var board = self // mutating
                board.play(mark: mark, at: coordinates)
                possible.append((coordinates,board))
            }
        }
        return possible
    }
    
    
    typealias Pattern = (a:Int,b:Int,c:Int)
    static let Patterns:[Pattern] = [ // winning patterns
        (0, 1, 2), (3, 4, 5), (6, 7, 8),
        (0, 3, 6), (1, 4, 7), (2, 5, 8),
        (0, 4, 8), (6, 4, 2)
    ]
    
    enum State {
        case Playing
        case Victory(mark:Mark,pattern:Pattern)
        case Draw
    }
    var state:State {
        for p:Pattern in Board.Patterns {
            
            let first = self[p.a]
            if  first != .E && (first == self[p.b] && self[p.b] == self[p.c]) {
                return .Victory(mark: first,pattern: p)
            }
        }
        if self.data.filter({ $0 == .E }).count == 0 {
            return .Draw
        }
        return .Playing
    }
}


/*
     │   │
   X │ X │ X
 ────┼───┼────
   X │ X │ X
 ────┼───┼────
   X │ X │ X
     │   │
 */

extension Board: CustomStringConvertible {
    public var description: String {
        var s:String = "\n"
        for row in 0..<Board.Height {
            let c0 = self[row,0].rawValue
            let c1 = self[row,1].rawValue
            let c2 = self[row,2].rawValue
            s.append("  \(c0) │ \(c1) │ \(c2)\n")
            
            if row < 2 {
                s.append("────┼───┼────\n")
            }
        }
        return s
    }
}


