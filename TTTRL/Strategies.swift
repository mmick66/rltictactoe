//
//  Behaviours.swift
//  TTTRL
//
//  Created by Michael Michailidis on 27/02/2017.
//  Copyright © 2017 Shenzhen Huazhongxiang. All rights reserved.
//

import Foundation


class Strategy: CustomStringConvertible {
    
    private(set) var mark:Mark!
    
    init(with mark:Mark) {
        self.mark = mark
    }
    func choose(in board:Board) -> Coordinates? {
        return nil
    }
    
    var description: String {
        return "Abstract"
    }
}



class RandomStrategy: Strategy {
    override func choose(in board:Board) -> Coordinates? {
        return board.expand(for: mark).random?.coordinates
    }
}

class BasicStrategy: Strategy {
    
    override func choose(in board:Board) -> Coordinates? {
        
        guard let oposite = mark.oposite else {
            fatalError("Mark is Empty")
        }
        
        // if there is a chance to win, take it
        
        if let winning = board.expand(for: mark).filter({
            
            switch $0.board.state {
                case .Victory(let mark, _): return mark == self.mark
                default: return false
            }
            
        }).first {
            
            return winning.coordinates
            
        }
        
        // if not... if there is a chance to block the oponent from winning do that...
        
        if let winning = board.expand(for: oposite).filter({
            
            switch $0.board.state {
                case .Victory(let mark, _): return mark == oposite
                default: return false
            }
            
        }).first {
            
            return winning.coordinates
            
        }
        
        return board.expand(for: mark).random?.coordinates
    }
    
    override var description: String {
        return "Basic"
    }
    
}



class MinimaxStrategy: Strategy {
    
    
    override func choose(in board:Board) -> Coordinates? {
        return self.minimax(for: self.mark, in: board).coordinates
    }
    
    
    struct Result {
        let score:Int
        let coordinates:Coordinates?
        init(_ score:Int, coordinates:Coordinates? = nil) {
            self.score          = score
            self.coordinates    = coordinates
        }
    }
    func minimax(for mark:Mark, in board:Board) -> Result {
        
        guard let oponent = mark.oposite else {
            fatalError("Cannot play with empty")
        }
        
        switch board.state {
        case .Victory(let mark,_):
            return Result(mark == self.mark ? 10 : -10)
        case .Draw: return Result(0)
        case .Playing: break
        }
        
        var options:[Result] = [Result]()
        for (c,b) in board.expand(for: mark) {
            
            let n:Result = self.minimax(for: oponent, in: b)
            let r:Result = Result(n.score, coordinates: c)
            options.append(r)
        }
        
        if mark == self.mark {
            return options.sorted(by: { $0.score > $1.score }).first!
        } else {
            return options.sorted(by: { $0.score < $1.score }).first!
        }
        
    }
    
    override var description: String {
        return "Minimax"
    }
    
}


class RLStrategy: Strategy {
    
    typealias ScoreFunction = ((Board)->Float)
    
    private class Table {
        
        var data:[String:Float] = [String:Float]()
        var score:ScoreFunction
        
        init(with score:@escaping ScoreFunction) {
            self.score = score
        }
        
        subscript(board:Board) -> Float {
            get {
                let h = hash(board)
                if data[h] == nil {
                    data[h] = self.score(board)
                }
                return data[h]!
            }
            set {
                let h = hash(board)
                self.data[h] = newValue
            }
        }
        
        func hash(_ board: Board) -> String {
            var s:String = ""
            for mark in board.data {
                s.append(mark.rawValue) // unique
            }
            return s
        }
    }
    
    static let Alpha:Float      = 0.8
    static let Epsilon:Float    = 0.1
    
    private(set) var epsilon:Float
    
    internal func score(for board: Board) -> Float {
        switch board.state {
        case .Playing:
            return 0.5
        case .Draw:
            return 0
        case .Victory(let mark, _):
            return mark == self.mark ? 1.0 : -1.0
        }
    }
    
    private var table:RLStrategy.Table!
    
    init(with mark: Mark, alpha:Float = RLStrategy.Alpha, epsilon: Float = RLStrategy.Epsilon) {
        
        self.epsilon = epsilon
        
        super.init(with: mark)
        
        self.table = RLStrategy.Table(with: self.score)
        
    }
    
    private var previous:Board?
    override func choose(in board:Board) -> Coordinates? {
        
        var expan = board.expand(for: mark)
        let moves = expan.shuffled.sorted { score(for: $0.board) > score(for: $1.board) }
        
        if moves.count == 0 {
            return nil
        }
        
        var next:Board.Move!
        
        if Float.random() < self.epsilon {
            
            next = moves.random!
            
        } else {
            
            next = moves.first!
            
            self.backtrack(from: next.board)
            
        }
        
        self.previous = next.board
        
        return next.coordinates
        
    }
    
    
    private func backtrack(from next: Board) {
        guard let previous = self.previous else {
            return
        }
        self.table[previous] += RLStrategy.Alpha * (self.table[next] - self.table[previous])
    }
    
    // MARK: API
    func finish(with board:Board) {
        
        self.table[board] = self.score(for: board)
        self.backtrack(from: board)
        
        self.previous = nil
        
    }
    
    override var description: String {
        return "Reinforcement Learning"
    }
    
    
}

