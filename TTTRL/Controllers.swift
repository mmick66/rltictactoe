//
//  ViewController.swift
//  TTTRL
//
//  Created by Michael Michailidis on 17/02/2017.
//  Copyright © 2017 Shenzhen Huazhongxiang. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    
    enum Segues:String {
        case ShowPlayingScreen      = "ShowPlayingScreen"
        case ShowTrainingScreen     = "ShowTrainingScreen"
        var identifier:String {
            return self.rawValue
        }
    }
    
    @IBOutlet weak var playRLButton: UIButton!
    @IBOutlet weak var playMinimaxButton: UIButton!
    @IBOutlet weak var playBasicButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.playBasicButton.addTarget(self, action: #selector(playBasicButtonPressed), for: .touchUpInside)
        self.playMinimaxButton.addTarget(self, action: #selector(playMinimaxButtonPressed), for: .touchUpInside)
        self.playRLButton.addTarget(self, action: #selector(playRLButtonPressed), for: .touchUpInside)
    }
    
    @objc func playBasicButtonPressed(button:UIButton) {
        self.performSegue(withIdentifier: HomeController.Segues.ShowPlayingScreen.identifier, sender: button)
    }
    
    @objc func playMinimaxButtonPressed(button:UIButton) {
        self.performSegue(withIdentifier: HomeController.Segues.ShowPlayingScreen.identifier, sender: button)
    }
    
    @objc func playRLButtonPressed(button:UIButton) {
        self.performSegue(withIdentifier: HomeController.Segues.ShowTrainingScreen.identifier, sender: button)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let button:UIButton = sender as? UIButton else {
            fatalError("Forgot to wire up the buttons in Storyboard")
        }
        
        
        if segue.identifier == HomeController.Segues.ShowPlayingScreen.identifier {
            
            guard let boardController = segue.destination as? BoardController else {
                fatalError("Wired up the wrong storyboard")
            }
            
            var ai:Player.AI!
            
            switch button {
            case self.playBasicButton:
                ai = Player.AI.Basic
            case self.playMinimaxButton:
                ai = Player.AI.Minimax
            default:
                break
            }
            
            boardController.oponent = Player(with: .O, and: ai)
            
        }
        
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

class TrainingController: UIViewController {
    
    static let Iterations = 100000
    
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    let trainer = Player(with: .X, and: Player.AI.Basic)
    let learner = Player(with: .O, and: Player.AI.Learned)
    
    @IBOutlet weak var randomScoreLabel: UILabel!
    @IBOutlet weak var aiScoreLabel: UILabel!
    
    private let bgQueue = DispatchQueue(label: "com.karmadust.trainingcontroller.bg.q")
    
    struct Result {
        let victory: Mark
        let history: [Board]
    }
    
    static let GoToGameSegueIdentifier = "GoToGame"
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        func iterate(for times:Int) -> Void {
            
            if times == 0 { // exit condition
                self.loader.stopAnimating()
                self.learner.score = 0
                self.performSegue(withIdentifier: TrainingController.GoToGameSegueIdentifier, sender: self)
                return
            }
            
            let update = { (result:Result) in
                
                [self.trainer, self.learner].filter({ $0.mark == result.victory }).first?.score += 1
                
                // update labels
                self.randomScoreLabel.text  = "Random: \(self.trainer.score)"
                self.aiScoreLabel.text      = "AI: \(self.learner.score)"
                
                if let rl_strategy = self.learner.strategy as? RLStrategy {
                    guard let board = result.history.last else {
                        fatalError("Empty history")
                    }
                    rl_strategy.finish(with: board)
                }
                
                iterate(for: times-1)
                
            }
            
            bgQueue.async {
                
                let result = self.play(trainer: self.trainer, learner: self.learner)
                
                // print(result.history)
                
                DispatchQueue.main.async {
                    update(result)
                }
                
            }
            
        } // ends: iterate
        
        self.loader.startAnimating()
        
        iterate(for: TrainingController.Iterations)
        
    }
    
    func play(trainer:Player, learner:Player) -> Result {
        
        var board:Board = Board()
        var player:Player = Float.random() > 0.5 ? trainer : learner
        var history:[Board] = []
        
        while true {
            
            let coordinates = player.choose(for: board)!
            
            board.play(mark: player.mark, at: coordinates)
            
            history.append(board)
            
            let s = board.state
            
            switch s {
                case .Victory(let mark, _):
                    return Result(victory: mark, history: history)
                case .Draw:
                    return Result(victory: .E, history: history)
                case .Playing:
                    player = (player === trainer) ? learner : trainer; continue
            }
            
        }
        
    }
    
    // Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == TrainingController.GoToGameSegueIdentifier {
            let vc = segue.destination as! BoardController
            vc.oponent = self.learner
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

class BoardController: UIViewController, UIGestureRecognizerDelegate {
    
    let player:Player   = Player(with: .X)
    var oponent:Player! // passed externally
    
    private let aiQueue = DispatchQueue(label: "com.karmadust.boardcontroller.ai.q")
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    private var current:Player? {
        didSet {
            if current != nil && current! == oponent {
                self.playOponent()
                
            }
        }
    }
    
    @IBOutlet weak var aiScoreLabel: UILabel!
    @IBOutlet weak var youScoreLabel: UILabel!
    
    private func oposite(player: Player) -> Player {
        if player == self.player {
            return self.oponent
        } else {
            return self.player
        }
    }
    
    var model:Board!

    @IBOutlet weak var gameView: BoardView!
    @IBOutlet weak var boardView: BoardView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let tapRecogniser       = UITapGestureRecognizer(target: self, action: #selector(BoardController.handleTap))
        tapRecogniser.delegate  = self
        self.boardView.addGestureRecognizer(tapRecogniser)
        
        
        self.loader.hidesWhenStopped = true
        self.loader.stopAnimating()
        
        self.startGame()
        
    }
    
    
    func startGame() {
        
        self.updateScore()
        
        self.model = Board()
        self.gameView.updateSubviews(with: self.model)
        
        self.current = self.player
    }
    
    // MARK: Gesture Recogniser Callbacks
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let current = self.current else {
            return false
        }
        return current == self.player
    }
    
    @objc private func handleTap(recogniser: UITapGestureRecognizer) {
        
        let location = recogniser.location(in: recogniser.view)
        
        let _findCoordinates = { (tap:CGPoint) -> (row: Int, col: Int)? in
            for row in 0..<Board.Height {
                for col in 0..<Board.Width {
                    let c = self.boardView[row,col]
                    if c.frame.contains(tap) {
                        return (row: row, col: col)
                    }
                }
            }
            return nil
        }
        
        guard let coordinates = _findCoordinates(location) else {
            return
        }
        
        if self.model[coordinates.row,coordinates.col] != .E {
            return
        }
        
        self.executePlay(for: self.player, at: coordinates)
        
    }
    
    private func playOponent() {
        
        self.loader.startAnimating()
        
        aiQueue.async {
            
            guard let choice = self.oponent.choose(for: self.model) else {
                fatalError("No choice")
            }
            
            DispatchQueue.main.async {
                self.loader.stopAnimating()
                self.executePlay(for: self.oponent, at: choice)
            }
        }
    }
    
    
    
    private func executePlay(for player: Player, at choice: Coordinates) {
        
        self.model.play(mark: player.mark, at: choice)
        self.boardView.updateSubviews(with: self.model)
        
        switch self.model.state {
        case .Victory(let mark, let pattern): return self.showVictory(of: mark, with: pattern)
        case .Draw:                           return self.boardView.clearBoard() { self.startGame() }
        case .Playing:                        break
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.current = self.oposite(player: player)
        }
        
        
    }
    
    private func showVictory(of mark:Mark, with pattern:Board.Pattern) {
        
        if mark == self.player.mark {
            self.player.score   += 1
        } else {
            self.oponent.score  += 1
        }
        
        self.boardView.showVictory(pattern) {
            self.boardView.clearBoard() {
                self.startGame()
            }
        }
    }
    
    private func updateScore() {
        self.youScoreLabel.text  = "You: \(self.player.score.string)"
        self.aiScoreLabel.text   = "Ai: \(self.oponent.score.string)"
    }
    
    private func checkVictory() {
        
    }
    
    func gestureRecognizerShouldBegin() -> Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }


}


